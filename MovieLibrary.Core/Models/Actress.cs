﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.Core.Models
{
    public class Actress
    {
        public int ActressId { get; set; }
        public string ActressName { get; set; }
        public string ActressAltName { get; set; }
        public string ActressPhoto { get; set; }
        public string ActressDoB { get; set; }
        public string ActressMeasurements { get; set; }
        public string ActressLink { get; set; }
    }
}

﻿using MovieLibrary.UWP.Helpers;
using MovieLibrary.UWP.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.UWP.ViewModels
{
    public class JapanPageVM : INotifyPropertyChanged
    {
        public ObservableCollection<GroupInfoList> JapanActressList { get; set; }

        public JapanPageVM()
        {
            JapanActressList = new ObservableCollection<GroupInfoList>();
            LoadData();
        }

        private void LoadData()
        {
            JapanActressList.Clear();

            var tempList = Actress.GetActressGrouped(LocalDataProvider.LoadJapanActresses());

            foreach(var item in tempList)
            {
                JapanActressList.Add(item);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string caller = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}

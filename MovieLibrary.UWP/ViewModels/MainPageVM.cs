﻿using MovieLibrary.UWP.Helpers;
using MovieLibrary.UWP.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.UWP.ViewModels
{
    public class MainPageVM : INotifyPropertyChanged
    {
        public ObservableCollection<NavPage> MainPageList { get; set; }

        private NavPage _selectedPage;

        public NavPage SelectedPage
        {
            get => _selectedPage;
            set
            {
                if (Equals(_selectedPage, value)) return;
                _selectedPage = value;
                OnPropertyChanged();
            }
        }

        public MainPageVM()
        {
            MainPageList = new ObservableCollection<NavPage>();
            LoadData();
        }

        private void LoadData()
        {
            MainPageList.Clear();
            var tempMainPageList = LocalDataServices.LoadPageList();
            foreach (var mainPage in tempMainPageList)
            {
                MainPageList.Add(mainPage);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string caller = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}

﻿using MovieLibrary.UWP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.ApplicationModel;

namespace MovieLibrary.UWP.Helpers
{
    public static class LocalDataServices
    {
        public static List<NavPage> LoadPageList()
        {
            List<NavPage> navItems = new List<NavPage>();
            XDocument config = XDocument.Load(Path.Combine(Package.Current.InstalledLocation.Path, @"Configurations\MainConfig.xml"));

            navItems = config
                        .Root
                        .Elements("NavigationView")
                        .Elements("NavItem")
                        .Select(x => new NavPage
                        {
                            Type = Type.GetType(x.Element("Type").Value),
                            Title = x.Element("Title").Value
                        }).ToList();
            return navItems;
        }
    }
}

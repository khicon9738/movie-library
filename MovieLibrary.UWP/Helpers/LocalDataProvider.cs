﻿using MovieLibrary.UWP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;

namespace MovieLibrary.UWP.Helpers
{
    public static class LocalDataProvider
    {
        public static List<Actress> LoadJapanActresses()
        {
            List<Actress> japanActresses = new List<Actress>()
            {
                new Actress
                {
                    ActressId = 0,
                    ActressName = "Mizuno Asahi",
                    ActressAltName="???",
                    ActressPhoto = Path.Combine(Package.Current.InstalledLocation.Path, @"Media\Japan\mizuno-asahi.jpg")
                },
                new Actress
                {
                    ActressId = 1,
                    ActressName = "JULIA",
                    ActressAltName="???",
                    ActressPhoto = Path.Combine(Package.Current.InstalledLocation.Path, @"Media\Japan\julia.jpg")
                }
            };

            return japanActresses;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.UWP.Models
{
    public class NavPage : INotifyPropertyChanged
    {
        private string _title;
        private Type _type;

        public string Title
        {
            get => _title;
            set
            {
                if (Equals(_title, value)) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        public Type Type
        {
            get => _type;
            set
            {
                if (Equals(_type, value)) return;
                _type = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged (string caller = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}

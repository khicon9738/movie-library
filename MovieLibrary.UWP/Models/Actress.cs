﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.UWP.Models
{
    public class Actress : INotifyPropertyChanged
    {
        private int _actressId;
        public int ActressId
        {
            get => _actressId;
            set
            {
                if (Equals(_actressId, value)) return;
                _actressId = value;
                OnPropertyChanged();
            }
        }

        private string _actressName;
        public string ActressName
        {
            get => _actressName;
            set
            {
                if (Equals(_actressName, value)) return;
                _actressName = value;
                OnPropertyChanged();
            }
        }

        private string _actressAltName;
        public string ActressAltName
        {
            get => _actressAltName;
            set
            {
                if (Equals(_actressAltName, value)) return;
                _actressAltName = value;
                OnPropertyChanged();
            }
        }

        private string _actressPhoto;
        public string ActressPhoto
        {
            get => _actressPhoto;
            set
            {
                if (Equals(_actressPhoto, value)) return;
                _actressPhoto = value;
                OnPropertyChanged();
            }
        }

        public static ObservableCollection<GroupInfoList> GetActressGrouped(List<Actress> listActress)
        {
            ObservableCollection<GroupInfoList> groups = new ObservableCollection<GroupInfoList>();

            var query = from actress in listActress
                        group actress by actress.ActressName.Substring(0, 1).ToUpper() into g
                        orderby g.Key
                        select new
                        {
                            GroupName = g.Key,
                            Actresses = g
                        };

            foreach (var g in query)
            {
                GroupInfoList info = new GroupInfoList
                {
                    Key = g.GroupName
                };
                foreach(var actress in g.Actresses)
                {
                    info.Add(actress);
                }
                groups.Add(info);
            }

            return groups;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string caller = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}
